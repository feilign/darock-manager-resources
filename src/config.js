/**

 @Name：全局配置
 @Author：贤心
 @Site：http://www.layui.com/admin/
 @License：LPPL（layui付费产品协议）
    
 */

layui.define(['laytpl', 'layer', 'element', 'util'], function (exports) {
  exports('setter', {
    container: 'LAY_app' //容器ID
      ,
    base: layui.cache.base //记录layuiAdmin文件夹所在路径
      ,
    views: layui.cache.base + 'views/' //视图所在目录
      ,
    entry: 'home/Hello' //默认视图文件名
      ,
    engine: '.html' //视图文件后缀名
      ,
    pageTabs: false //是否开启页面选项卡功能。单页版不推荐开启
      ,
    tokenstr: 'L2BFlpggNEA=',
    name: '管理系统',//左侧的菜单栏的表名
    tableName: 'OilDepotFindOil2' //本地存储表名
      ,
    MOD_NAME: 'admin' //模块事件名

      ,
    debug: false //是否开启调试模式。如开启，接口异常时会抛出异常 URL 等信息

      ,
    interceptor: true //是否开启未登入拦截
      ,
  //  url: 'http://112.29.173.226:5001'
      url: 'https://youku.yunfeiyang.com/DMSInterface',
      //自定义请求字段
      url1: 'http://112.29.173.226:5001'
      // url1: 'https://af9c073a-da76-4f46-87fc-fd5731fa160c.mock.pstmn.io'
      ,
    request: {
      tokenName: false //自动携带 token 的字段名。可设置 false 不携带。
    }

    //自定义响应字段
    ,
    response: {
      statusName: 'code' //数据状态的字段名称
        ,
      statusCode: {
        ok: 0 //数据状态一切正常的状态码
          ,
        logout: 1001 //登录状态失效的状态码
      },
      msgName: 'msg' //状态信息的字段名称
        ,
      dataName: 'data' //数据详情的字段名称
    }
    // ,response: {
    //   statusName: 'Result' //规定数据状态的字段名称，默认：code
    //   , msgName: 'Msg' //规定状态信息的字段名称，默认：msg
    //   , dataName: 'Data' //规定数据列表的字段名称，默认：data
    // },


    //独立页面路由，可随意添加（无需写参数）
    ,
    indPage: [
        '/user/login' //登入页
      ]

      //扩展的第三方模块
      ,
    extend: [
        'echarts', //echarts 核心包
        'echartsTheme'
        // , //echarts 主题
        // 'soulTable',
        // 'tableFilter',
        // 'excel',
        // 'soulTable.slim',
        // 'tableChild',
        // 'tableMerge'
      ]

      //主题配置
      ,
    theme: {
      //内置主题配色方案
      color: [{
          main: '#344058',
          logo: '#0085E8',
          selected: '#1E9FFF',
          header: '#1E9FFF',
          alias: 'ocean-header' //海洋头
        }, {
          main: '#20222A' //主题色
            ,
          selected: '#009688' //选中色
            ,
          alias: 'default' //默认别名
        }, {
          main: '#03152A',
          selected: '#3B91FF',
          alias: 'dark-blue' //藏蓝
        }, {
          main: '#2E241B',
          selected: '#A48566',
          alias: 'coffee' //咖啡
        }, {
          main: '#50314F',
          selected: '#7A4D7B',
          alias: 'purple-red' //紫红
        }, {
          main: '#344058',
          logo: '#1E9FFF',
          selected: '#1E9FFF',
          alias: 'ocean' //海洋
        }, {
          main: '#3A3D49',
          logo: '#2F9688',
          selected: '#5FB878',
          alias: 'green' //墨绿
        }, {
          main: '#20222A',
          logo: '#F78400',
          selected: '#F78400',
          alias: 'red' //橙色
        }, {
          main: '#28333E',
          logo: '#AA3130',
          selected: '#AA3130',
          alias: 'fashion-red' //时尚红
        }, {
          main: '#24262F',
          logo: '#3A3D49',
          selected: '#009688',
          alias: 'classic-black' //经典黑
        }, {
          logo: '#226A62',
          header: '#2F9688',
          alias: 'green-header' //墨绿头
        }, {
          header: '#393D49',
          alias: 'classic-black-header' //经典黑
        }, {
          main: '#50314F',
          logo: '#50314F',
          selected: '#7A4D7B',
          header: '#50314F',
          alias: 'purple-red-header' //紫红头
        }, {
          main: '#28333E',
          logo: '#28333E',
          selected: '#AA3130',
          header: '#AA3130',
          alias: 'fashion-red-header' //时尚红头
        }, {
          main: '#28333E',
          logo: '#009688',
          selected: '#009688',
          header: '#009688',
          alias: 'green-header' //墨绿头
        }]

        //初始的颜色索引，对应上面的配色方案数组索引
        //如果本地已经有主题色记录，则以本地记录为优先，除非请求本地数据（localStorage）
        ,
      initColorIndex: 11
    },
    l_page: {
      layout: ['prev', 'page', 'next', 'skip', 'count', 'limit'] //自定义分页布局
        ,
      curr: 1 //设定初始在第 5 页
      ,theme:'#009688'
    },
    l_limit: 10,
    l_limits: [10, 15, 20, 25, 30],
    l_response: {
      statusName: 'Code' //规定数据状态的字段名称，默认：code
        ,
      statusCode: 200 //规定成功的状态码，默认：0
        ,
      msgName: 'Msg' //规定状态信息的字段名称，默认：msg
        ,
      dataName: 'Data' //规定数据列表的字段名称，默认：data
      ,countName: 'Count' //规定数据总数的字段名称，默认：count
    }
  });
});