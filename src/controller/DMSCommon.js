function getSearchParamCommon(_SearchData) {
    var l_SearchJson = {
        "Token": {
            "OperatorName": "",
            "OperatorNo": "",
            "DepotNo": "",
            "SystemType": "",
            "TokenStr": layui.setter.tokenstr,
            "Version": "1.0"
        },
        "Data": _SearchData
    };
    return l_SearchJson;
}

function CreateAction(ActionList) {
    // debugger;
    if (ActionList == null || ActionList == "" || ActionList == "null") {
        $(".btn-tool").removeClass("displaynone");
    }
    else {
        var l_ActionListArr = ActionList.split(",");
        for (var i = 0; i < l_ActionListArr.length; i++) {
            $(".actionbtn" + l_ActionListArr[i]).css("display", "inline");
        }
    }
}

//跳转连接
function jumpDetaliUrl(v_Title, v_url, v_Height) {
    var l_height = '600px';
    if (v_Height)
        l_height = v_Height;
    var Url = {
        type: 1,
        title: v_Title,
        area: ['800px', l_height],
        shade: [0.8, '#393D49'],
        skin: 'my-btn', //加上边框
        shadeClose: false,
        anim: 5,
        resize: false,
        maxmin: false, //关闭最大化最小化按钮
        success: function (index, layero) {
            layui.view(this.id).render(v_url);
        },
        end: function () {
        }
    };
    return Url;
}

//获取最大编号
function GetMaxNo(url) {
    var l_MaxNo = "";
    var l_SearchNoJson = {
        "Token": {
            "OperatorName": "",
            "OperatorNo": "",
            "DepotNo": "",
            "SystemType": "",
            "TokenStr": layui.setter.tokenstr,
            "Version": "1.0"
        }
    };
    l_SearchNoJson = JSON.stringify(l_SearchNoJson);
    $.ajax({
        url: layui.setter.url + url,
        type: 'POST', //GET
        async: false,    //或false,是否异步
        data: l_SearchNoJson,
        contentType: 'application/json',
        dataType: 'json',    //返回的数据格式：json/xml/html/script/jsonp/text
        success: function (data) {
            try {
                l_MaxNo = data.Data.maxNo;
            } catch (e) {
                // alert(e.Message);
            }
        }
    });
    return l_MaxNo;
}

//押运员下拉
function EscortSelect(v_ID, v_CustNo, v) {
    var l_SearchJson = {
        "Token": {
            "OperatorName": "",
            "OperatorNo": "",
            "DepotNo": "",
            "SystemType": "",
            "TokenStr": layui.setter.tokenstr,
            "Version": "1.0"
        },
        "Data": {
            "CustNo": v_CustNo
        }
    };
    l_SearchJson = JSON.stringify(l_SearchJson);
    $.ajax({
        url: layui.setter.url + '/OutDepot/GetEscortMessageInfo',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: l_SearchJson,
        async: false,
        success: function (data) {
            //debugger;
            var l_Result = data.Data;
            var str = "<option value='-1'>请选择</option>";
            for (var i = 0; i < l_Result.length; i++) {
                if (v && $.trim(v) == l_Result[i].escortNo) {
                    str += "<option value='" + l_Result[i].escortNo + "' selected>" + l_Result[i].escortName + "</option>";
                } else
                    str += "<option value='" + l_Result[i].escortNo + "'>" + l_Result[i].escortName + "</option>";

            }
            $("#" + v_ID).html(str);
        }
    });
}

function RoleInit(v_ID, v) {
    //创建获取油品信息的json  文档里面有
    var l_SearchJson = {
        "Token": {
            "OperatorName": "",
            "OperatorNo": "",
            "DepotNo": "",
            "SystemType": "",
            "TokenStr": layui.setter.tokenstr,
            "Version": "1.0"
        },
        "Data": {
           
        }
    };

    l_SearchJson = JSON.stringify(l_SearchJson);
    $.ajax({
        url: layui.setter.url + '/BaseInfo/GetOperatorRolesInfo',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: l_SearchJson,
        async: false,
        success: function (data) {
            //debugger
            var l_Result = data.Data;
            $("#" + v_ID).append("<option value='-1'>请选择</option>");
            for (var i = 0; i < l_Result.length; i++) {
                var str = "<option value='" + l_Result[i].roleID + "'>" + l_Result[i].roleName + "</option>";
                if (v && $.trim(v) == l_Result[i].roleID) {
                    str = "<option value='" + l_Result[i].roleID + "' selected>" + l_Result[i].roleName + "</option>";
                }
                $("#" + v_ID).append(str);
            }
        }
    });
}

function GetCustStockByTankNo(v_TankNo, v_CustNo) {
    var OilModel;
    var l_SearchOilJson = {
        "Token": {
            "OperatorName": "",
            "OperatorNo": "",
            "DepotNo": "",
            "SystemType": "",
            "TokenStr": layui.setter.tokenstr,
            "Version": "1.0"
        },
        "Data": {
            "TankNo": v_TankNo,
            "CustNo": v_CustNo
        }
    };

    l_SearchOilJson = JSON.stringify(l_SearchOilJson);
    $.ajax({
        url: layui.setter.url+ '/Bussiness/GetCustStockByTankNo',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: l_SearchOilJson,
        async: false,
        success: function (data) {
            var l_Result = data.Result;
            if (l_Result == 0)
                OilModel = data.Data[0];
        }
    });
    return OilModel;
}

//通过客户编号查询客户model
function getCustModelbyCustNo(v_CustNo) {
    var Model;
    var l_SearchOilJson = {
        "Token": {
            "OperatorName": "",
            "OperatorNo": "",
            "DepotNo": "",
            "SystemType": "",
            "TokenStr": layui.setter.tokenstr,
            "Version": "1.0"
        },
        "Data": {
            "CustNo": v_CustNo
        }
    };

    l_SearchOilJson = JSON.stringify(l_SearchOilJson);
    $.ajax({
        url:layui.setter.url + '/BaseInfo/GetCustInfo',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: l_SearchOilJson,
        async: false,
        success: function (data) {
            //debugger;
            var l_Result = data.Data;
            if (l_Result.length > 0)
                Model = l_Result[0];
        }
    });
    return Model;
}
//司机下拉
function DriverSelect(v_ID, v_CustNo, v) {
    var l_SearchJson = {
        "Token": {
            "OperatorName": "",
            "OperatorNo": "",
            "DepotNo": "",
            "SystemType": "",
            "TokenStr": layui.setter.tokenstr,
            "Version": "1.0"
        },
        "Data": {
            "CustNo": v_CustNo
        }
    };
    l_SearchJson = JSON.stringify(l_SearchJson);
    $.ajax({
        url:layui.setter.url + '/OutDepot/GetDriverMessageInfo',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: l_SearchJson,
        async: false,
        success: function (data) {
            //debugger;
            var l_Result = data.Data;
            var str = "<option value='-1'>请选择</option>";
            for (var i = 0; i < l_Result.length; i++) {
                str += "<option value='" + l_Result[i].driverNo + "'>" + l_Result[i].driverName + "</option>";
                if (v && $.trim(v) == l_Result[i].driverNo) {
                    str += "<option value='" + l_Result[i].driverNo + "' selected>" + l_Result[i].driverName + "</option>";
                }
            }
            $("#" + v_ID).html(str);
        }
    });
}

//车辆下拉
function CarSelect(v_ID, v_CustNo, v) {
    var l_SearchJson = {
        "Token": {
            "OperatorName": "",
            "OperatorNo": "",
            "DepotNo": "",
            "SystemType": "",
            "TokenStr": layui.setter.tokenstr,
            "Version": "1.0"
        },
        "Data": {
            "CustNo": v_CustNo
        }
    };
    l_SearchJson = JSON.stringify(l_SearchJson);
    $.ajax({
        url: layui.setter.url  + '/OutDepot/GetCarMessageInfo',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: l_SearchJson,
        async: false,
        success: function (data) {
            //debugger;
            var l_Result = data.Data;
            var str = "<option value='-1'>请选择</option>";
            for (var i = 0; i < l_Result.length; i++) {
                str += "<option value='" + l_Result[i].carNo + "'>" + l_Result[i].carNo + "</option>";
                if (v && $.trim(v) == l_Result[i].carNo) {
                    str += "<option value='" + l_Result[i].carNo + "' selected>" + l_Result[i].carNo + "</option>";
                }
            }
            $("#" + v_ID).html(str);
        }
    });
}

//油品下拉
function OilInit(v_OilID, v) {
    //创建获取油品信息的json  文档里面有
    var l_SearchOilJson = {
        "Token": {
            "OperatorName": "",
            "OperatorNo": "",
            "DepotNo": "",
            "SystemType": "",
            "TokenStr": layui.setter.tokenstr,
            "Version": "1.0"
        },
        "Data": {
            "Used": 0
        }
    };

    l_SearchOilJson = JSON.stringify(l_SearchOilJson);
    $.ajax({
        url:layui.setter.url + '/SysDict/GetOilInfo',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: l_SearchOilJson,
        async: false,
        success: function (data) {
            //debugger;
            var l_Result = data.Data;
            $("#" + v_OilID).empty();
            $("#" + v_OilID).append("<option value='-1'>请选择</option>");
            for (var i = 0; i < l_Result.length; i++) {
                var str = "<option value='" + l_Result[i].oilNo + "'>" + l_Result[i].oilAbbr + "</option>";
                if (v && $.trim(v) == l_Result[i].oilNo) {
                    str = "<option value='" + l_Result[i].oilNo + "' selected>" + l_Result[i].oilAbbr + "</option>";
                }
                $("#" + v_OilID).append(str);
            }
        }
    });
}
//各种状态下拉
function StatusInit(v_ID, v_ItemType, v) {
    //创建获取油品信息的json  文档里面有
    var l_SearchJson = {
        "Token": {
            "OperatorName": "",
            "OperatorNo": "",
            "DepotNo": "",
            "SystemType": "",
            "TokenStr":layui.setter.tokenstr,
            "Version": "1.0"
        },
        "Data": {
            "ItemType": v_ItemType
        }
    };

    l_SearchJson = JSON.stringify(l_SearchJson);
    $.ajax({
        url: layui.setter.url  + '/SysDict/GetType',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: l_SearchJson,
        async: false,
        success: function (data) {
            var l_Result = data.Data;
            var str = "<option value='-1'>请选择</option>";
            for (var i = 0; i < l_Result.length; i++) {
                if (v != null && $.trim(v) == l_Result[i].itemID) {
                    str += "<option value='" + l_Result[i].itemID + "' selected>" + l_Result[i].itemName + "</option>";
                } else {
                    str += "<option value='" + l_Result[i].itemID + "'>" + l_Result[i].itemName + "</option>";
                }
            }
            $("#" + v_ID).html(str);
        }
    });
}
//根据油罐编号获取油品相关信息
function getOilModelbyTankNo(v_TankNo) {
    var OilModel;
    var l_SearchOilJson = {
        "Token": {
            "OperatorName": "",
            "OperatorNo": "",
            "DepotNo": "",
            "SystemType": "",
            "TokenStr": layui.setter.tokenstr,
            "Version": "1.0"
        },
        "Data": {
            "TankNo": v_TankNo
        }
    };

    l_SearchOilJson = JSON.stringify(l_SearchOilJson);
    $.ajax({
        url: layui.setter.url + '/SysDict/GetTankInfo',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: l_SearchOilJson,
        async: false,
        success: function (data) {
            //debugger;
            var l_Result = data.Data;
            if (l_Result.length > 0)
                OilModel = l_Result[0];
        }
    });
    return OilModel;
}


//客户下拉
function CustInit(v_ID, v) {
    //创建获取油品信息的json  文档里面有
    var l_SearchJson = {
        "Token": {
            "OperatorName": "",
            "OperatorNo": "",
            "DepotNo": "",
            "SystemType": "",
            "TokenStr": layui.setter.tokenstr,
            "Version": "1.0"
        },
        "Data": {}
    };
    l_SearchJson = JSON.stringify(l_SearchJson);
    $.ajax({
        url: layui.setter.url + '/BaseInfo/GetCustInfo',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: l_SearchJson,
        async: false,
        success: function (data) {
            //debugger;
            var l_Result = data.Data;
            $("#" + v_ID).empty();
            $("#" + v_ID).append("<option value='-1'>请选择</option>");
            for (var i = 0; i < l_Result.length; i++) {
                var str = "<option value='" + l_Result[i].custNo + "'>" + l_Result[i].customerName + "</option>";
                if (v && $.trim(v) == l_Result[i].custNo) {
                    str = "<option value='" + l_Result[i].custNo + "' selected>" + l_Result[i].customerName + "</option>";
                }
                $("#" + v_ID).append(str);
            }
        }
    });
}

//新油罐下拉
function TankSelect(dom_id, value) {
    //创建获取油品信息的json  文档里面有
    var l_SearchJson = {
        "Token": {
            "OperatorName": "",
            "OperatorNo": "",
            "DepotNo": "",
            "SystemType": "",
            "TokenStr": layui.setter.tokenstr,
            "Version": "1.0"
        },
        "Data": {}
    };

    l_SearchJson = JSON.stringify(l_SearchJson);
    $.ajax({
        url: layui.setter.url + '/SysDict/GetTankInfo',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: l_SearchJson,
        async: false,
        success: function (res) {
            var data = res.Data;
            var str = "<option value='-1'>请选择</option>";
            for (var i = 0; i < data.length; i++) {
                if (value && $.trim(value) == data[i].tankNo) {
                    str += "<option value='" + data[i].tankNo + "' selected>" + data[i].tankName + "</option>";
                } else
                    str += "<option value='" + data[i].tankNo + "'>" + data[i].tankName + "</option>";
            }
            $("#" + dom_id).html(str);
        }
    });
}
//油罐下拉
function TankInit(v_ID, v) {
    //创建获取油品信息的json  文档里面有
    var l_SearchJson = {
        "Token": {
            "OperatorName": "",
            "OperatorNo": "",
            "DepotNo": "",
            "SystemType": "",
            "TokenStr": layui.setter.tokenstr,
            "Version": "1.0"
        },
        "Data": {}
    };

    l_SearchJson = JSON.stringify(l_SearchJson);
    $.ajax({
        url:  layui.setter.url + '/SysDict/GetTankInfo',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: l_SearchJson,
        async: false,
        success: function (data) {
            //debugger;
            var l_Result = data.Data;
            $("#" + v_ID).empty();
            $("#" + v_ID).append("<option value='-1'>请选择</option>");
            for (var i = 0; i < l_Result.length; i++) {
                var str = "<option value='" + l_Result[i].tankNo + "'>" + l_Result[i].tankName + "</option>";
                if (v && $.trim(v) == l_Result[i].tankNo) {
                    str = "<option value='" + l_Result[i].tankNo + "' selected>" + l_Result[i].tankName + "</option>";
                }
                $("#" + v_ID).append(str);
            }
        }
    });
}




//提交各种增删改
function CRUDFunction(v_Field, v_Url) {
    var l_ResultData;
    var l_SearchJson = {
        "Token": {
            "OperatorName": "",
            "OperatorNo": "",
            "DepotNo": "",
            "SystemType": "",
            "TokenStr": layui.setter.tokenstr,
            "Version": "1.0"
        },
        "Data": v_Field
    };

    l_SearchJson = JSON.stringify(l_SearchJson);
    $.ajax({
        url: layui.setter.url + v_Url,
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: l_SearchJson,
        async: false,
        success: function (data) {
            l_ResultData = data;
        }
    });
    return l_ResultData;
}


function IsDigit(v) {
    var id = v.id;
    var regExp = /^\d{1,12}(\.(\d{1,3})?)?$/;
    if (document.getElementById(id).value != "") {
        if (!regExp.test(document.getElementById(id).value)) {
            var Str = document.getElementById(id).value.toString();
            //alert("只能输入整数或小数，最多可以输入3位小数,12位整数！");
            alert("只能输入整数或小数，最多可以输入3位小数,12位整数!");
            document.getElementById(id).value = "";
            return false;
        }
    }
}
//时间格式化
function dateFtt(fmt, date) {
    var o = {
        "M+": date.getMonth() + 1,     //月份
        "d+": date.getDate(),     //日
        "h+": date.getHours(),     //小时
        "m+": date.getMinutes(),     //分
        "s+": date.getSeconds(),     //秒
        "q+": Math.floor((date.getMonth() + 3) / 3), //季度
        "S": date.getMilliseconds()    //毫秒
    };
    if (/(y+)/.test(fmt))
        fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt))
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}


function GetUserInfoByPhoneNo(PhoneNo) {
    var OilModel = null;
    var l_SearchOilJson = {
        "Token": {
            "OperatorName": "",
            "OperatorNo": "",
            "DepotNo": "",
            "SystemType": "",
            "TokenStr": layui.setter.tokenstr,
            "Version": "1.0"
        },
        "Data": {
            "PhoneNo": PhoneNo
        }
    };

    l_SearchOilJson = JSON.stringify(l_SearchOilJson);
    $.ajax({
        url: layui.setter.url + '/BaseInfo/GetUserInfoByPhoneNo',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: l_SearchOilJson,
        async: false,
        success: function (data) {
            var l_Result = data.Result;
            if (l_Result == 0)
                OilModel = data.Data[0];
        }
    });
    return OilModel;
}
function StringParsefloat(v) {
    if (v == null || v == "")
        v = '0';
    return parseFloat(v);
}
//文本框限制最大长度
function max_lenght(value, length) {
    if (value.length > length) {
        console.log(value.length);
        value = value.slice(0, length)
    }
    return value;
}
function plusXing(str, frontLen, endLen) {
    var len = str.length - frontLen - endLen;
    var xing = '';
    for (var i = 0; i < len; i++) {
        xing += '*';
    }
    return str.substring(0, frontLen) + xing + str.substring(str.length - endLen);
}